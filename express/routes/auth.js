const {Router} = require('express')
const User = require('../models/user')
const bcrypt = require('bcryptjs')
const {validationResult} = require('express-validator')
const {validateRegister} = require('../utils/validators')

const router = Router()

router.get('/login', async (req, res) => {
  res.render('auth/login', {
    title: 'Авторизация',
    isLogin: true,
    errorLogin: req.flash('errorLogin'),
    errorRegister: req.flash('errorRegister'),
  })
})

router.get('/logout', async (req, res) => {
  req.session.destroy(() => {
    res.redirect('/auth/login#login')
  })
})

router.post('/login', async (req, res) => {
  try{
    const {email, password} = req.body;
    const candidate = await User.findOne({ email })
    if (candidate) {
      const areSame = await bcrypt.compare(password, candidate.password);

      if(areSame) {
        req.session.user = candidate;
        req.session.isAuthenticated = true;
        req.session.save(err => {
          if (err) throw err
          res.redirect('/')
        })
      } else {
        req.flash('errorLogin', 'Пароль неверный');
        res.redirect('/auth/login#login')
      }
    } else {
      req.flash('errorLogin', 'Ппользователя нету');
      res.redirect('/auth/login#login')
    }

  } catch (e) {
    console.log('-----> ', e);
  }
})

router.post('/register', validateRegister, async (req, res) => {
  try{
    const {email, password, name} = req.body;

    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.flash('errorRegister', errors.array()[0].msg)
      return res.status(422).redirect('/auth/login#register')
    }

    const passwordHash = await bcrypt.hash(password, 10)
    const user = new User({
      email, password: passwordHash, name, cart: {items: []}
    })

    await user.save();
    res.redirect('/auth/login#login')

  } catch (e) {
    console.log('-----> ', e);
  }
})

module.exports = router