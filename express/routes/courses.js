const {Router} = require('express')
const Course = require('../models/course')
const authMiddleware = require('../middleware/auth')
const {validateCourse} = require('../utils/validators')
const {validationResult} = require('express-validator')

const router = Router()

router.get('/', async (req, res) => {
  const courses = await Course.find({}).populate('userId', 'name email').select('title price img').lean();
  
  res.render('courses', {
    title: 'Courses',
    isCourse: true,
    courses
  })
})

router.get('/:id', async (req, res) => {
  const course = await Course.findById(req.params.id).lean()
  res.render('course', {
    layout: 'empty',
    course
  })
})

router.get('/:id/edit', authMiddleware, async (req, res) => {
  if (!req.query.allow) {
    return res.redirect('/')
  }

  const course = await Course.findById(req.params.id).lean()

  res.render('course-edit', {
    title: `Редактировать курс ${course.title}`,
    course
  })
})

router.post('/edit', authMiddleware, validateCourse, async (req, res) => {
  const errors = validationResult(req);
  const {id} = req.body;

  if (!errors.isEmpty()){
    return res.status(422).redirect(`/courses/${id}/edit?allow=true`)
  }

  await Course.findByIdAndUpdate(id, req.body)
  res.redirect('/courses')
})

router.post('/remove', authMiddleware, async (req, res) => {
  await Course.deleteOne({_id: req.body.id})
  res.redirect('/courses')
})

module.exports = router