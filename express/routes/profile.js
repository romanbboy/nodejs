const {Router} = require('express')
const authMiddleware = require('../middleware/auth')
const User = require('../models/user')
const router = Router();

router.get('/', authMiddleware, async (req, res) => {
  res.render('profile', {
    title: 'Profile',
    user: req.user.toObject()
  })
})

router.post('/', authMiddleware, async (req, res) => {
  const user = await User.findById(req.user._id)
  
  const toChange = {
    name: req.body.name
  }
  

  if(req.file){
    toChange.avatarUrl = req.file.path
  }

  Object.assign(user, toChange);

  await user.save();
  res.redirect('/profile')
})

module.exports = router

