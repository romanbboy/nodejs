const {Router} = require('express')
const Order = require('../models/order')
const authMiddleware = require('../middleware/auth')

const router = Router()

router.get('/', authMiddleware, async (req, res) => {
  try {
    const orders = await Order.find({'user.userId': req.user}).populate('user.userId').lean()
    
    const ordersTransform = orders.map(o => ({
      ...o,
      price: o.courses.reduce((total, course) => {
        return total += course.count * course.course.price;
      }, 0)
    }))
    
    res.render('orders', {
      title: 'Заказы',
      isOrders: true,
      orders: ordersTransform
    })
  } catch (e) {
    console.log('-----> ', e);
  }
})

router.post('/', authMiddleware, async (req, res) => {
  try {
    const user = await req.user.populate('cart.items.courseId').execPopulate();
    const courses = user.cart.items.map(i => ({
      count: i.count,
      course: {...i.courseId._doc}
    }))

    const order = new Order({
      user: {
        name: req.user.name,
        userId: req.user
      },
      courses
    })

    await order.save();

    await req.user.clearCart();

    res.redirect('/orders')
  } catch (e) {
    console.log('-----> ', e);
  }
})

module.exports = router