const {Router} = require('express')
const Course = require('../models/course')
const authMiddleware = require('../middleware/auth')

const router = Router()

const mapCartItems = (items) => {
  return items.map(c => ({
    ...c.courseId._doc,
    count: c.count
  }))
}

const computedPrice = courses => {
  return courses.reduce((total, course) => {
    return total += course.price * course.count;
  }, 0)
}

router.get('/', authMiddleware, async (req, res) => {
  const user = await req.user.populate('cart.items.courseId').execPopulate()
  const courses = mapCartItems(user.cart.items)

  res.render('card', {
    title: 'Корзина',
    isCard: true,
    courses,
    price: computedPrice(courses)
  })
})

router.post('/add', authMiddleware, async (req, res) => {
  const course = await Course.findById(req.body.id)
  await req.user.addToCart(course)
  res.redirect('/card')
})

router.delete('/remove/:id', authMiddleware, async (req, res) => {
  await req.user.removeCourse(req.params.id)
  const user = await req.user.populate('cart.items.courseId').execPopulate()
  const courses = mapCartItems(user.cart.items)
  const cart = {
    courses,
    price: computedPrice(courses)
  }

  res.status(200).json(cart)
})

module.exports = router