const {body} = require('express-validator')
const User = require('../models/user')

exports.validateRegister = [
  body('email', 'Неверный email')
    .isEmail()
    .custom(async (value, {req}) => {
      const user = await User.findOne({email: value})

      if(user){
        // return Promise.reject('Пользователь такой есть епта')
        throw new Error('Пользователь такой есть епта2')
      }

      return true;
    })
    .normalizeEmail(),
  body('password', 'Пароль минимум 3 символов')
    .isLength({min: 3, max:10})
    .isAlphanumeric()
    .trim(),
  body('confirm')
    .custom((value, {req}) => {
      if(value !== req.body.password){
        throw new Error('Пароли должны совпадать')
      }

      return true;
    })
    .trim(),
  body('name', 'Имя минимум 3 символа')
    .isLength({min: 3})
    .trim()
]

exports.validateCourse = [
  body('title', 'Название минимум 3 символа').isLength({min: 3}).trim(),
  body('price', 'Только цифры').isNumeric(),
  body('img', 'Корректный url').isURL(),
]